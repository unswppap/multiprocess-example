import multiprocessing
import time


# class must extend multiprocessing.Process
class CalculationProcess(multiprocessing.Process):
    # all arguments could be passed at initializer
    def __init__(self, index, circles):
        # must call parent's init
        super().__init__()
        # then do what we need
        self.index = index
        self.circles = circles
        self.duration = None

    # write all it needs to do in this function
    def run(self):
        start = time.time()
        value = 0
        # pretend to do some calculation
        # in real time, all codes in this function is the algorithms
        for i in range(self.circles):
            value += 1
        end = time.time()
        self.duration = end - start
        print(f'process#{self.index} took {self.duration}s')


if __name__ == '__main__':
    # create a instance of this class
    calculateProcess1 = CalculationProcess(1, 10000000)
    calculateProcess2 = CalculationProcess(2, 10000000)
    # run it by calling its start()
    calculateProcess1.start()
    calculateProcess2.start()
